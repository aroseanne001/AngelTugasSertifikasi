//
//  BooksListView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct BooksListView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(sortDescriptors: [SortDescriptor(\.title)]) var books: FetchedResults<Book>
    
    @AppStorage("didLaunchBefore") var didLaunchBefore: Bool = false
    
    var body: some View {
        NavigationView {
            List {
                ForEach(books) {
                    book in
                    NavigationLink {
                        BookDetailView(book: book)
                    } label: {
                        BookRowCardView(book: book)
                    }
                }
            }
            .navigationTitle("Katalog Buku 📚")
            .onLoad {
                if !didLaunchBefore {
                    addDefaultBooks()
                    didLaunchBefore = true
                }
                
            }
        }
    }
    
    func addDefaultBooks() {
        // default books
        let b = Book(context: moc)
        b.id = "01"
        b.title = "Finding Me"
        b.publishedAt = "2022"
        b.author = "Viola Davis"
        b.img = "one"
        b.desc = "The multiple award-winning actress describes the difficulties she encountered before claiming her sense of self and achieving professional success."
        
        let b2 = Book(context: moc)
        b2.id = "02"
        b2.title = "The Palace Papers"
        b2.publishedAt = "2021"
        b2.author = "Tina Brown"
        b2.img = "two"
        b2.desc = "This follow-up to “The Diana Chronicles” details how the royal family reinvented itself after the death of Princess Diana."
        
        let b3 = Book(context: moc)
        b3.id = "03"
        b3.title = "Unmasked"
        b3.publishedAt = "2020"
        b3.author = "Paul Holes; Robin Gaby Fisher"
        b3.img = "three"
        b3.desc = "A memoir by a former cold case investigator who worked on several notable cases, including the identification of the Golden State Killer."
        
        let b4 = Book(context: moc)
        b4.id = "04"
        b4.title = "Freezing Order"
        b4.publishedAt = "2020"
        b4.author = "Bill Browder"
        b4.img = "four"
        b4.desc = "The author of “Red Notice” tells his story of becoming Vladimir Putin’s enemy by uncovering a $230 million tax refund scheme."
        
        let b5 = Book(context: moc)
        b5.id = "05"
        b5.title = "I'll Show Myself Out"
        b5.publishedAt = "2020"
        b5.author = "Jessi Klein"
        b5.img = "five"
        b5.desc = "A collection of comedic essays on motherhood and middle age by the Emmy Award-winning writer and producer."
        
        let b6 = Book(context: moc)
        b6.id = "06"
        b6.title = "Dream Town"
        b6.publishedAt = "2020"
        b6.author = "David Baldacci"
        b6.img = "six"
        b6.desc = "The third book in the Archer series. Archer, Dash and Callahan search for a missing screenwriter who had a dead body turn up in her home."
        
        let b7 = Book(context: moc)
        b7.id = "07"
        b7.title = "Run, Rose, Run"
        b7.publishedAt = "2021"
        b7.author = "James Patterson; Dolly Parton"
        b7.img = "seven"
        b7.desc = "A singer-songwriter goes to Nashville seeking stardom but is followed by her dark past."
        
        let b8 = Book(context: moc)
        b8.id = "08"
        b8.title = "City on Fire"
        b8.publishedAt = "2022"
        b8.author = "Don Winslow"
        b8.img = "eight"
        b8.desc = "Two rival crime families that control all of New England start a war against each other."
        
        let b9 = Book(context: moc)
        b9.id = "09"
        b9.title = "The Good Left Undone"
        b9.publishedAt = "2020"
        b9.author = "Adriana Trigiani"
        b9.img = "nine"
        b9.desc = "The matriarch of the Cabrelli family, who is near the end of her life, shares her mother’s love story with her family."
        
        let b10 = Book(context: moc)
        b10.id = "10"
        b10.title = "Sea of Tranquility"
        b10.publishedAt = "2020"
        b10.author = "Emily St. John Mandel"
        b10.img = "ten"
        b10.desc = "A detective investigating in the wilderness discovers that his actions might affect the timeline of the universe."
        
        do {
            try moc.save()
        } catch {}
    }
}

struct BooksListView_Previews: PreviewProvider {
    static var previews: some View {
        BooksListView()
    }
}
