//
//  BookRowCardView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct BookRowCardView: View {
    @ObservedObject var book: Book
    
    var body: some View {
        HStack {
            Image(book.wrappedBookImg)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 45)
            
            VStack(alignment: .leading) {
                Text(book.wrappedBookTitle)
                    .font(.body.bold())
                Text("\(book.wrappedAuthor) - \(book.wrappedPublishedAt)")
                    .font(.footnote)
            }
            .padding()
            Spacer()
            if book.isBorrowed {
                Image(systemName: "person.crop.circle.badge.clock")
                    .foregroundColor(.accentColor)
                    .font(.title3)
            }
        }
    }
}
