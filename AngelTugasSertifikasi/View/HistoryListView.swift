//
//  HistoryListView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct HistoryListView: View {
    @Environment(\.managedObjectContext) var moc

    @FetchRequest(sortDescriptors: [SortDescriptor(\.userId)]) var users: FetchedResults<User>
    
    var body: some View {
        NavigationView {
            List {
                ForEach(users, id: \.self) { user in
                    NavigationLink {
                        HistoryDetailView(user: user)
                    } label: {
                        HistoryRowView(user: user)
                    }
                }
                
            }
            .navigationTitle("Catatan Peminjaman🗒")
        }
    }
}

struct HistoryListView_Previews: PreviewProvider {
    static var previews: some View {
        HistoryListView()
    }
}
