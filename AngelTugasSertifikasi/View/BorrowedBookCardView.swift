//
//  BorrowedBookCardView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct BorrowedBookCardView: View {
    @ObservedObject var borrowedBook: Book
    let dateFormatter = DateFormatter()
    @State var dateString = ""
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(borrowedBook.wrappedBookTitle)
                    .font(.headline.bold())
                Text("Tgl. Pengembalian \(dateString)")
                    .font(.footnote)
            }
            Spacer()
            if borrowedBook.isBorrowed {
                Image(systemName: "square")
                    .foregroundColor(.red)
                    .padding(.horizontal)
            } else {
                    Image(systemName: "checkmark.square")
                        .foregroundColor(.green)
                        .padding(.horizontal)
            }
        }
        .onAppear {
            dateFormatter.dateFormat = "MMM dd, yyyy"
            dateString = dateFormatter.string(from: borrowedBook.wrappedReturnDate)
        }
    }
}
