//
//  HistoryRowView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct HistoryRowView: View {
    @ObservedObject var user: User
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(user.wrappedUserId) - \(user.wrappedUserName)")
                .font(.body.bold())
            
            Text("\(user.borrowedBooksArray.reduce(0) { $0 + ($1.isBorrowed ? 1 :0 )}) buku sedang dipinjam")
                .font(.footnote)
        }
        .padding()
    }
}
