//
//  BookDetailView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct BookDetailView: View {
    // book detail
    @ObservedObject var book: Book
    @State var showBorrowForm = false
    
    // form pinjaman
    @State var username = ""
    @State var userId = ""
    @State private var showBorrowAlert = false
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) var moc
    let dateFormatter = DateFormatter()
    @State var dateString = ""
    
    var body: some View {
        ScrollView {
            HStack {
                VStack(alignment: .leading) {
                    Text("Author : \(book.wrappedAuthor)")
                        .font(.headline)
                    Text("Published Year : \(book.wrappedPublishedAt)")
                        .font(.headline)
                    Divider()
                    Text("Description : ")
                        .font(.headline)
                    Text(book.wrappedBookDesc)
                        .font(.body)
                }
                .padding()
                Spacer()
            }
            if book.isBorrowed {
                Text("Maaf, Buku Sedang Dipinjam")
                    .font(.body.bold())
                    .padding()
                    .foregroundColor(.primary)
                    .background(.secondary)
                    .cornerRadius(10)
            } else {
                Button {
                    showBorrowForm = true
                } label: {
                    Text("Form Pinjaman Buku")
                        .font(.body.bold())
                        .padding()
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .cornerRadius(10)
                }
            }
            
            if showBorrowForm {
                VStack(alignment: .leading) {
                    Text("Masukkan Nama Anda : ")
                        .font(.body.bold())
                    TextField("Budi", text: $username)
                        .padding(.horizontal)
                        .frame(height: 50)
                        .background(Color(.systemGray6))
                        .cornerRadius(10)
                    
                    Text("Masukkan Nomor Anggota Anda : ")
                        .font(.body.bold())
                    TextField("123456", text: $userId)
                        .padding(.horizontal)
                        .frame(height: 50)
                        .background(Color(.systemGray6))
                        .cornerRadius(10)
                    
                    HStack {
                        Spacer()
                        Button{
                            if userId.count > 0 && username.count > 0{
                                updateBookStatus()
                            }
                        } label:{
                            Text("Pinjam Buku")
                                .font(.body.bold())
                                .padding()
                                .foregroundColor(.white)
                                .background(Color.blue)
                                .cornerRadius(10)
                        }
                        .alert("Buku Berhasil Dipinjam :) Kembalikan sebelum \(dateString)", isPresented: $showBorrowAlert) {
                            Button("Saya Mengerti", role: .cancel) { }
                        }
                        Spacer()
                    }
                    .padding()
                }
                .padding()
            }
        }
        .navigationTitle("\(book.wrappedBookTitle) 📒")
    }
    
    func updateBookStatus() {
        let addDays = 7
        let newDate = Calendar.current.date(byAdding: .day, value: addDays, to: Date())
        
        dateFormatter.dateFormat = "MMM dd, yyyy"
        dateString = dateFormatter.string(from: newDate ?? Date())
        
        self.book.objectWillChange.send()
        self.book.isBorrowed = true
        self.book.borrowedBy = User(context: moc)
        self.book.borrowedBy?.userId = userId
        self.book.borrowedBy?.name = username
        self.book.borrowDate = Date()
        self.book.returnDate = newDate
        
        do {
            try moc.save()
        } catch {}
        
        self.book.objectWillChange.send()
        
        showBorrowAlert = true
        presentationMode.wrappedValue.dismiss()
    }
}
