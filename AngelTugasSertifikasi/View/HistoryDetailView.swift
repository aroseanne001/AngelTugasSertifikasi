//
//  HistoryDetailView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct HistoryDetailView: View {
    @ObservedObject var user: User
    @Environment(\.managedObjectContext) var moc
    
    var body: some View {
        List {
            Section(header:
                        Text("Koleksi Buku yang Dipinjam")) {
                
                ForEach(user.borrowedBooksArray) {
                    borrowedBook in
                    BorrowedBookCardView(borrowedBook: borrowedBook)
                        .onTapGesture {
                            user.objectWillChange.send()
                            borrowedBook.objectWillChange.send()
                            borrowedBook.isBorrowed = !borrowedBook.isBorrowed
                            do {
                                try moc.save()
                            } catch {}
                        }
                }
            }
        }
        .listStyle(.plain)
        .navigationTitle("\(user.wrappedUserName)")
    }
}
