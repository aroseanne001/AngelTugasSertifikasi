//
//  TabsView.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//

import SwiftUI

struct TabsView: View {
    var body: some View {
        TabView {
            BooksListView()
                .tabItem {
                    Label("Katalog Buku", systemImage: "books.vertical")
                }
            HistoryListView()
                .tabItem {
                    Label("Catatan Peminjaman", systemImage: "list.bullet.rectangle.portrait")
                }
        }
    }
}

struct TabsView_Previews: PreviewProvider {
    static var previews: some View {
        TabsView()
    }
}
