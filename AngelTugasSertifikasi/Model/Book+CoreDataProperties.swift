//
//  Book+CoreDataProperties.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//
//

import Foundation
import CoreData


extension Book {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Book> {
        return NSFetchRequest<Book>(entityName: "Book")
    }

    @NSManaged public var id: String?
    @NSManaged public var title: String?
    @NSManaged public var author: String?
    @NSManaged public var publishedAt: String?
    @NSManaged public var desc: String?
    @NSManaged public var isBorrowed: Bool
    @NSManaged public var borrowDate: Date?
    @NSManaged public var returnDate: Date?
    @NSManaged public var img: String?
    @NSManaged public var borrowedBy: User?

    
    public var wrappedBookId: String {
        id ?? "None"
    }
    
    public var wrappedBookTitle: String {
        title ?? "None"
    }
    
    public var wrappedAuthor: String {
        author ?? "None"
    }
    
    public var wrappedPublishedAt: String {
        publishedAt ?? "None"
    }
    
    public var wrappedBookDesc: String {
        desc ?? "None"
    }
    
    public var wrappedBorrowDate: Date {
        borrowDate ?? Date()
    }
    
    public var wrappedReturnDate: Date {
        returnDate ?? Date()
    }
    
    public var wrappedBookImg: String {
        img ?? "None"
    }
    
}

extension Book : Identifiable {

}
