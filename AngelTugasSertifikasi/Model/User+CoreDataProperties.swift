//
//  User+CoreDataProperties.swift
//  AngelTugasSertifikasi
//
//  Created by Angelica Roseanne on 07/05/22.
//
//

import Foundation
import CoreData

extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var userId: String?
    @NSManaged public var name: String?
    @NSManaged public var borrowBooks: NSSet?

    public var wrappedUserId: String {
        userId ?? "None"
    }
    
    public var wrappedUserName: String {
        name ?? "None"
    }
    
    public var borrowedBooksArray: [Book] {
        let set = borrowBooks as? Set<Book> ?? []
        
        return set.sorted {
            $0.wrappedBookTitle < $1.wrappedBookTitle
        }
    }
    
}

// MARK: Generated accessors for borrowBooks
extension User {

    @objc(addBorrowBooksObject:)
    @NSManaged public func addToBorrowBooks(_ value: Book)

    @objc(removeBorrowBooksObject:)
    @NSManaged public func removeFromBorrowBooks(_ value: Book)

    @objc(addBorrowBooks:)
    @NSManaged public func addToBorrowBooks(_ values: NSSet)

    @objc(removeBorrowBooks:)
    @NSManaged public func removeFromBorrowBooks(_ values: NSSet)

}

extension User : Identifiable {

}
